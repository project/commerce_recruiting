<?php

/**
 * @file
 * Describes hooks and plugins provided by the module.
 */

use Drupal\commerce_recruiting\Entity\CampaignInterface;

/**
 * Hook to allow modules to alter the set of recruitments added to the reward.
 *
 * @param array &$recruitments
 *   List of recruitments that will be added to the reward.
 * @param \Drupal\commerce_recruiting\Entity\CampaignInterface $campaign
 *   The current campaign.
 */
function hook_recruitment_reward_recruitments_alter(array &$recruitments, CampaignInterface $campaign) {
}

/**
 * Hook to allow modules to alter the set of recruitments used in a summary.
 *
 * @param array &$recruitments
 *   List of recruitments that will be used to create a summary.
 * @param \Drupal\commerce_recruiting\Entity\CampaignInterface $campaign
 *   The current campaign.
 */
function hook_recruitment_summary_recruitments_alter(array &$recruitments, CampaignInterface $campaign) {
}
