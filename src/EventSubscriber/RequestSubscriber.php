<?php

namespace Drupal\commerce_recruiting\EventSubscriber;

use Drupal\commerce_recruiting\CampaignManagerInterface;
use Drupal\commerce_recruiting\Code;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for acting on requests.
 */
class RequestSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The campaign service.
   *
   * @var \Drupal\commerce_recruiting\CampaignManagerInterface
   */
  protected CampaignManagerInterface $campaignManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a new RecruitmentCheckoutSubscriber object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(AccountProxy $current_user, CampaignManagerInterface $campaign_manager, Messenger $messenger, LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->campaignManager = $campaign_manager;
    $this->messenger = $messenger;
    $this->logger = $logger->get('commerce_recruiting');
    $this->config = $config_factory->get('commerce_recruiting.settings');  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onRequest'],
    ];
  }

  /**
   * Event handler on request.
   *
   * Saves recruitment session if option is enabled and parameter is set.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function onRequest(RequestEvent $event) {
    if ($this->config->get('use_recruitment_code_url_parameter') !== 1) {
      // Recruitment code url parameter option is disabled.
      return;
    }

    $recruitment_code_parameter_name = $this->config->get('recruitment_code_url_parameter_name');
    if (!$event->getRequest()->query->has($recruitment_code_parameter_name)) {
      // Query not set.
      return;
    }

    $campaign_code = $event->getRequest()->query->get($recruitment_code_parameter_name);
    $code = Code::createFromCode($campaign_code);

    try {
      $recruiter = $this->campaignManager->getRecruiterFromCode($code);
      $option = $this->campaignManager->findCampaignOptionFromCode($code);

      if ($recruiter->id() == $this->currentUser->id() && (!$option->getCampaign()->hasField('allow_self_recruit') || !$option->getCampaign()->allow_self_recruit->value)) {
        \Drupal::messenger()->addMessage(t('You can not use your own recommendation url.'));
      }
      else {
        $this->campaignManager->saveRecruitmentSession($code);
      }
    }
    catch (\Throwable $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError(t('Invalid Code. If you believe this to be an error please contact us.'));
    }
  }

}
