<?php

namespace Drupal\commerce_recruiting\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Commerce Recruiting settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_recruiting_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_recruiting.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['use_recruitment_code_url_parameter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use recruitment codes as query parameter'),
      '#description' => $this->t('Enable option to support recruitment codes as query parameters (/?code=abc--123)'),
      '#default_value' => $this->config('commerce_recruiting.settings')->get('use_recruitment_code_url_parameter') ?? FALSE,
    ];

    $form['recruitment_code_url_parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recruitment code URL parameter name'),
      '#description' => $this->t('Optionally change the parameter name for recruitment codes. Only lower case a-z allowed.'),
      '#default_value' => $this->config('commerce_recruiting.settings')->get('recruitment_code_url_parameter_name') ?? 'code',
      '#states' => [
        'visible' => [
          [
            ':input[name="use_recruitment_code_url_parameter"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $form['write_recruitment_transition_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Write recruitment transition logs'),
      '#description' => $this->t('Disable option if recruitment transition errors should not be logged.'),
      '#default_value' => $this->config('commerce_recruiting.settings')->get('write_recruitment_transition_log') ?? TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('use_recruitment_code_url_parameter')) {
      $url_parameter_name = $form_state->getValue('recruitment_code_url_parameter_name');
      if (empty($url_parameter_name)) {
        $form_state->setError($form['recruitment_code_url_parameter_name'], $this->t('@name field is required.', ['@name' => $form['recruitment_code_url_parameter_name']['#title']]));
      }
      elseif (!ctype_lower($url_parameter_name)) {
        $form_state->setError($form['recruitment_code_url_parameter_name'], $this->t('Invalid format. Only lower case a-z allowed.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_recruiting.settings')
      ->set('use_recruitment_code_url_parameter', $form_state->getValue('use_recruitment_code_url_parameter'))
      ->set('recruitment_code_url_parameter_name', $form_state->getValue('recruitment_code_url_parameter_name'))
      ->set('write_recruitment_transition_log', $form_state->getValue('write_recruitment_transition_log'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
