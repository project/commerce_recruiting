<?php

namespace Drupal\commerce_recruiting\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Recruitment Summary' block.
 *
 * @Block(
 *  id = "commerce_recruiting_summary",
 *  admin_label = @Translation("Recruitment Summary block"),
 *  context_definitions = {
 *    "user" = @ContextDefinition("entity:user", required = TRUE)
 *  }
 * )
 */
class RecruitmentSummaryBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route;

  /**
   * The campaign manager.
   *
   * @var \Drupal\commerce_recruiting\CampaignManagerInterface
   */
  protected $campaignManager;

  /**
   * Loaded campaign.
   *
   * @var \Drupal\commerce_recruiting\Entity\CampaignInterface[]
   */
  private $campaigns;

  /**
   * The recruitment manager.
   *
   * @var \Drupal\commerce_recruiting\RecruitmentManagerInterface
   */
  private $recruitmentManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->languageManager = $container->get('language_manager');
    $instance->route = $container->get('current_route_match');
    $instance->campaignManager = $container->get('commerce_recruiting.campaign_manager');
    $instance->recruitmentManager = $container->get('commerce_recruiting.recruitment_manager');

    return $instance;
  }

  /**
   * Returns the recruitment summary block build.
   *
   * @return array
   *   The build array.
   */
  public function build() {
    $campaigns = $this->findCampaigns();
    $user = $this->getContextValue('user');
    $summaries = [];
    foreach ($campaigns as $campaign) {
      foreach (
        [
        'accepted' => 'accepted',
         'created' => 'pending',
        ] as $state => $key_name) {
        /* @var \Drupal\commerce_recruiting\Entity\CampaignInterface $campaign */
        $summary = $this->recruitmentManager->getRecruitmentSummaryByCampaign($campaign, $state, $user);
        if ($summary->hasResults()) {
          $summaries[$campaign->id()][$key_name] = $summary;
        }
      }
    }
    return  [
      '#theme' => 'recruitment_summary',
      '#summaries' => $summaries,
    ];
  }

  /**
   * Helper method to find the current matching campaign.
   *
   * @return \Drupal\commerce_recruiting\Entity\CampaignInterface|mixed|null
   *   The campaign or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function findCampaigns() {
    if (empty($this->campaigns)) {
      $user = $this->getContextValue('user');
      $global_campaigns = $this->campaignManager->findRecruiterCampaigns();
      $assigned_campaigns = $this->campaignManager->findRecruiterCampaigns($user);
      $this->campaigns = array_merge($global_campaigns, $assigned_campaigns);
    }
    return $this->campaigns;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getContextValue('user');
    if ($user->isAnonymous()) {
      return AccessResult::forbidden();
    }

    return parent::blockAccess($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
