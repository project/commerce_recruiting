<?php

namespace Drupal\commerce_recruiting\Plugin\Block;

use Drupal\commerce_recruiting\Code;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Product sharing link' block.
 *
 * @Block(
 *  id = "commerce_recruiting_friend",
 *  admin_label = @Translation("Product sharing link block"),
 *  context_definitions = {
 *    "entity" = @ContextDefinition("entity", required = TRUE),
 *    "user" = @ContextDefinition("entity:user", required = TRUE)
 *  }
 * )
 */
class FriendBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route;

  /**
   * The campaign manager.
   *
   * @var \Drupal\commerce_recruiting\CampaignManagerInterface
   */
  protected $campaignManager;

  /**
   * Loaded campaign.
   *
   * @var \Drupal\commerce_recruiting\Entity\CampaignInterface|null
   */
  private $campaign;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->languageManager = $container->get('language_manager');
    $instance->campaignManager = $container->get('commerce_recruiting.campaign_manager');
    $instance->route = $container->get('current_route_match');

    return $instance;
  }

  /**
   * Returns the block build array with a recruitment url to share.
   *
   * @return array
   *   The build array.
   */
  public function build() {
    /* @var \Drupal\commerce_recruiting\Entity\CampaignInterface $campaign */
    $campaign = $this->findCampaign();

    if (empty($campaign)) {
      // Nothing found.
      return [];
    }

    $recruiter_code = $this->getContextValue('user')->id();
    $user = User::load($recruiter_code);
    if ($user->hasField('code') && !empty($user->code->value)) {
      $recruiter_code = $user->code->value;
    }

    /* @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getContextValue('entity');
    foreach ($campaign->getOptions() as $option) {
      /* @var \Drupal\commerce_recruiting\Entity\CampaignOptionInterface $option */
      if ($option->getProduct()->id() == $entity->id() && $option->getProduct()->getEntityTypeId() == $entity->getEntityTypeId()) {
        $url = Code::create($option->getCode(), $recruiter_code)->url()->toString();
        $build['#theme'] = 'friend_share_block';
        $build['#share_link'] = $url;
        $build['#option'] = $option;
        return $build;
      }
    }

    return [];
  }

  /**
   * Helper method to find the current matching campaign.
   *
   * @return \Drupal\commerce_recruiting\Entity\CampaignInterface|mixed|null
   *   The campaign or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function findCampaign() {
    if (empty($this->campaign)) {
      $entity = $this->getContextValue('entity');
      if (!empty($entity)) {
        $campaigns = $this->campaignManager->findNoRecruiterCampaigns($entity);
      }

      // @todo: campaigns need at least one option with product. A general solution is not supported at this point.
      /*if (empty($campaigns)) {
        $campaigns = $this->campaignManager->findNoRecruiterCampaigns();
      }*/

      if (!empty($campaigns)) {
        $this->campaign = current($campaigns);
      }
    }
    return $this->campaign;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getContextValue('user');
    if ($user->isAnonymous()) {
      return AccessResult::forbidden();
    }

    if (empty($this->findCampaign())) {
      return AccessResult::forbidden();
    }

    return parent::blockAccess($account);
  }

}
