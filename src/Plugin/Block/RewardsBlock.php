<?php

namespace Drupal\commerce_recruiting\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Recruitment Rewards' block.
 *
 * @Block(
 *  id = "commerce_recruiting_rewards",
 *  admin_label = @Translation("Recruitment Rewards block"),
 *  context_definitions = {
 *    "user" = @ContextDefinition("entity:user", required = TRUE)
 *  }
 * )
 */
class RewardsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route;

  /**
   * The campaign manager.
   *
   * @var \Drupal\commerce_recruiting\CampaignManagerInterface
   */
  protected $campaignManager;


  /**
   * Loaded campaign.
   *
   * @var \Drupal\commerce_recruiting\Entity\RewardInterface[]
   */
  private $rewards = [];

  /**
   * The reward manager.
   *
   * @var \Drupal\commerce_recruiting\RewardManagerInterface
   */
  private $rewardManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->languageManager = $container->get('language_manager');
    $instance->campaignManager = $container->get('commerce_recruiting.campaign_manager');
    $instance->route = $container->get('current_route_match');
    $instance->rewardManager = $container->get('commerce_recruiting.reward_manager');

    return $instance;
  }

  /**
   * Returns the rewards block build.
   *
   * @return array
   *   The build array.
   */
  public function build() {
    $rewards = $this->findRewards();
    return  [
      '#theme' => 'recruitment_rewards',
      '#rewards' => $rewards,
    ];
  }

  /**
   * Helper method to lazy load rewards by current user.
   *
   * @return \Drupal\commerce_recruiting\Entity\RewardInterface|\Drupal\commerce_recruiting\Entity\RewardInterface[]
   *   List of rewards.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function findRewards() {
    if (!empty($this->rewards)) {
      return $this->rewards;
    }
    else {
      $user = $this->getContextValue('user');
      $this->rewards = $this->rewardManager->findRewards($user);
      return $this->rewards;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getContextValue('user');
    if ($user->isAnonymous()) {
      return AccessResult::forbidden();
    }
    if (count($this->findRewards()) === 0) {
      return AccessResult::forbidden();
    }
    return parent::blockAccess($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
