# Commerce Recruiting
This module extends Drupal's Commerce module to allow referral marketing via personalized links to share
with others. After a product purchase the recruiter who shared their link will receive a reward that can be redeemed.

## Features
- Group one or more products in a campaign
- Time limited campaigns
- Campaigns can be reserved for specific users only (like influencers, bloggers etc.)
- Customizable product and user codes
- Custom redirection path for each product code
- Users receive a recruitment per purchase that can be redeemed
- Auto re-recruit option: This will create subsequent recruitments each time the customer orders a product, if they have been recruited once before in a campaign.
- A link sharing block to append on pages for products that are free to share for everyone
- A block to show all campaigns with products and links of the current user
- Supports product bundles

## Basic concept & Usage
1. Go to `/admin/commerce/recruitment/campaigns` to create a new campaign. A campaign contains a set of products that
can be recommended with optional 'participants' of the campaign and optional date range. No recruiters mean the campaign
is open for all and a code will be available for all logged-in users (more about this see 'Available blocks').
2. Create one or more campaign options in the campaign. A campaign option defines which products can be recommended and
how much bonus the recruiter will be rewarded with.
3. A recommendation URL looks like this: `[DOMAIN]/code/[CAMPAIGN-OPTION-CODE]--[USER]`. The code consists of a campaign
option code and a user code or **UID**. Both the campaign option code and user code can be customized. A campaign option
code will be randomly generated on being created, where the user code is not. If there is no custom code set for a user,
the **UID** is used instead.
4. When the user calls the recommendation URL, they will be redirected to the product page by default, but the redirect
destination can also be set in the campaign option.
5. When the user adds the product to the cart, a recruiting info is stored in the order item (for detailed information
see section "Understanding how the recruiting info is stored" below).
6. On completing the checkout an actual recruitment is created for each order item, which has a recruiting info.
The recruitment consists of the following:

- the recruiter ("Who has recommended?")
- the recruited user / customer ("Who has purchased?")
- campaign option ("What has been purchased?")
- the bonus

All recruitments are listed here: `/admin/commerce/recruitment/recruitings`

7. Recruitments are created in the state 'Created' meaning 'pending' until they are 'accepted' or 'canceled' for any
reason. The state will be set to 'accepted' via **cron**, if the order is 'completed'.
8. Accepted recruitments are collected and can be redeemed anytime by the recruiter. To redeem recruitments, the
recruiter goes to `/user/rewards/collect/{campaign}`. Recruitments are redeemed per campaign. A 'reward' will be created
for the recruiter, which contains all affecting recruitments and the total of their bonus.
9. The reward as well as its recruitments are marked as 'pending'. After the reward was paid out, the state of the
reward state can be changed to 'paid'.

## Understanding how the recruiting info is stored
Calling the recommendation URL, the info about the campaign option and the recruiter are saved to the user's current
**session**. When the user adds a product the cart that matches the product from the session, a "**recruiting info**" is
stored in the order item. This is to make the recruitment persist even if the user starts a new session before they
actually complete the checkout. The matching also works in reversed order 1) user adds product to cart 2) user calls the
recommendation URL.

The recruiting info knows the campaign option and the recruiter.

Please note that there is currently **only one session info**. Calling a second recommendation URL will
**overwrite** the first. In other words: The customer can only be active to one campaign at a time and
only one recruiter per order will be rewarded.

## Available blocks
Note that all blocks are available for logged-in users only.

- Product sharing link block (FriendBlock): Will render a recommendation link on product pages, that are part of a campaign without any predefined recruiters. Intended for general 'free for all' campaigns. Note: Select `Product from URL` as entity value before saving the block!
- Recruiter campaigns sharing link block (RecruiterBlock): Will show a list of all campaign codes, that the current user is a part of. Codes are grouped by campaign. Optionally also shows the 'free for all' campaign codes.
- Recruitment Summary block: Shows a list of existing recruitments for the current user, grouped by campaign and state. Only shows created (pending) and accepted recruitments. Only accepted recruitments can be redeemed to a reward.
- Recruitment Rewards block: Renders a list of rewards of the current user.
- Recruitment Session Info: Shows information about the currently set recruitment session. Created for debuggung purposes.

## Additional options

A general settings form can be found at `/admin/commerce/config/recruiting/settings`.

- Use recruitment codes as query parameter: Enable option to support recruitment codes as URL parameters (/?code=abc--123)
- If query parameter option is enabled, you may change the parameter name for recruitment codes. Only lower case a-z allowed.
- Write recruitment transition logs: Disable option if recruitment transition errors should not be logged. Default: On
